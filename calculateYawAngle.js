	
autowatch = 1;

function calculateYawAngle(x, y, z){
    // Parse the magnetometer data along x, y, and z axes
    // post("ARG_1: " + x)
	// post("ARG_2: " + y)
	// post("ARG_3: " + z)

    // Calculate the yaw angle using the atan2 function
    var yaw = Math.atan2(y, x);

    // Convert yaw angle to degrees
    var yawDegrees = yaw * (180 / Math.PI);

    // Ensure yaw angle is within [0, 360] range
    yawDegrees = (yawDegrees + 360) % 360;

    // Return the yaw angle in degrees
    post(yawDegrees);
}
