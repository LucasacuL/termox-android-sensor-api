"""Small example OSC client

This program sends data from the mmc5603x Magnetometer Non-wakeup sensor to the /filter address.
"""
import argparse
import json
import subprocess
import time

from pythonosc import udp_client

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", default="192.168.2.2",
                        help="The IP of the OSC server")
    parser.add_argument("--port", type=int, default=5005,
                        help="The port the OSC server is listening on")
    args = parser.parse_args()

    client = udp_client.SimpleUDPClient(args.ip, args.port)

    while True:
        try:
            process = subprocess.Popen(['termux-sensor', '-s', 'mmc5603x Magnetometer Non-wakeup', '-n', '1'],
                                       stdout=subprocess.PIPE)
            output, _ = process.communicate()
            sensor_data = json.loads(output.decode())
            magnetometer_data = sensor_data['mmc5603x Magnetometer Non-wakeup']['values']
            print(magnetometer_data)
            client.send_message("/filter", magnetometer_data)
            time.sleep(0.1)  # Adjust the interval as needed
        except Exception as e:
            print("Error:", e)
            time.sleep(1)
